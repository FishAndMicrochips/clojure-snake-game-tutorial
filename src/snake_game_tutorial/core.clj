(ns snake-game-tutorial.core
  (:gen-class)
  (:import
   ;; Useful function from the widget toolkit
   (java.awt Color Dimension)
   ;; Bits of GUI
   (javax.swing JPanel JFrame Timer JOptionPane)
   ;; Get the key events and put them in an event loop
   (java.awt.event ActionListener KeyListener KeyEvent)))

;;;;==========================
;;;; MACROS

;; If you're confused as to what this does, look for >>>ME to see how this is used.
(defmacro defs
  "A simple macro which allows you to define multiple variables in 1 statement.\n
   Does it by destructuring the first set of variables in the list,\n
   then recursing until there are no more variables to define."
  [& names-values]
  `(do ~@((fn [[name value & rst] acc]
            (let [acc `(~@acc
                        (def ~name ~value))]
              (if (empty? rst)
                acc
                (recur rst acc))))
          names-values nil)))

;;;;============================
;;;; FUNCTIONAL MODEL
;;;---------------------------
;;; CONSTANTS
;; >>>ME
(defs
  ;; Parameters for the game board
  field-width 50
  field-height 30
  point-size 15
  ;; Parameters for the snake
  turn-millis 100
  winner-length 10 ;; If we eat 10 apples, we win! \o/
  ;; The cardinal directions expressed as unit vectors
  left [-1 0]
  right [1 0]
  up [0 -1]
  down [0 1]
  ;; Mapping key events to directions
  directions {KeyEvent/VK_LEFT left
              KeyEvent/VK_RIGHT right
              KeyEvent/VK_UP up
              KeyEvent/VK_DOWN down})

;;;--------------------------
;;; CONSTRUCTORS

(defn create-snake
  "For now, here's a simple map to hold our data.\n
   Records would probably be more efficient, but who cares?"
  []
  {:body (list [3 0] [2 0] [1 0] [0 0]) ;; A list of points. Eating an apple appends a new point
   :direction right
   :type :snake ;; Used to differentiate between apples and snakes
   :colour (Color. 15 160 70)}) ;; "Color." is a binding for Java's "new", so we're making "new Color(15, 160, 70);"

(defn create-apple []
  {:location [(rand-int field-width) (rand-int field-height)]
   :colour (Color. 210 50 90)
   :type :apple})

;;;-----------------------------
;;; Updaters. These might look like they are mutating, but they are really making a copy of a PERSISTENT data structure
;;; A Persistent data structure uses a tree in memory, so when you make a copy of a PDS,
;;;    you are re-using the vast majority of the original memory.
;;; Because data structures are immutable in Clojure,
;;;    you don't need to worry about some other function mutating that memory in place.

(defn move
  "Given the current head and direction, add a new point in that direction, from the head.\n
   If the snake isn't growing, cut off the very last element."
  [{:keys [body direction] :as snake} & growing?]
  ;; Return a snake with a different value of :body
  (assoc snake
         :body (cons
                ;; Make a new point moving in the direction of the snake's current trajectory
                (let [[head-x head-y] (first body)
                      [dir-x dir-y] direction]
                  [(+ head-x dir-x) (+ head-y dir-y)])
                ;; If we're not growing, remove the last point.
                (if growing?
                  body
                  (butlast body)))))

(defn turn
  "Replace the current trajectory the snake is taking"
  [snake direction]
  (assoc snake :direction direction))

;;;--------------------------------------
;;; Functional GUI Utilities

(defn point-to-screen-rect [[pt-x pt-y]]
  [(* pt-x point-size) ;; X on the GUI canvas
   (* pt-y point-size) ;; Y on the GUI canvas
   point-size ;; Width of a point
   point-size]) ;; Height of a point

(defn win? [{body :body}]
  (>= (count body) winner-length))

(let [head-overlaps-body?
      (fn [head body]
        (contains? (set body) head))

      head-outside-bounds?
      (fn [[head-x head-y]]
        (not (and (<= 0 head-x field-width)
                  (<= 0 head-y field-height))))]
  (defn lose?
    "We lose the game if the head goes out of bound, or if the snake eats itself."
    [{[head & body] :body}]
    (or (head-overlaps-body? head body)
        (head-outside-bounds? head))))

(defn eats?
  "To eat an apple, simply move the head to it. Simple as."
  [{[head] :body} {apple :location}]
  (= head apple))

;;;;=================================
;;;; IMPURE SECTION

(defn update-positions
  "Note that snake and apple, in this case, are both refs.\n
   We use refs because, in case the GUI and the core of the game are on separate threads,\n
   refs use software transactional memory to ensure that the GUI paints a consistent game state."
  [snake apple]
  (dosync
   (if (eats? @snake @apple) ;; @ is short for deref
     (do (ref-set apple (create-apple)) ;; We only have one apple on the board at a time
         (alter snake move :grow))
     (alter snake move)))
  nil)

(defn update-direction [snake direction]
  (dosync (alter snake turn direction))
  nil)

(defn reset-game [snake apple]
  (dosync (ref-set snake (create-snake))
          (ref-set apple (create-apple))) ;; We only have one apple on the board at a time
  nil)

;;;;=================================
;;;; GUI

;;;----------------------------------
;;; Painting onto a canvas

(defn fill-point
  "At a point given by [x y], draw a point on the screen according to our function which turns points to rectangles."
  [g pt color]
  ;; 'graphics context' is an object which contains methods which, when invoked, will draw within a surface
  ;; that is associated wit the graphics context.
  (let [[x y width height] (point-to-screen-rect pt)]
    ;; Here, we invoke some java methods which physically draw on our context.
    (.setColor g color)
    (.fillRect g x y width height)))

(defmulti paint
  "defmulti creates a multimethod -- a method which dispatches on type. It's like Common Lisp's 'defgeneric',\n
   or abstract classes in C++. In this case, we're making a generic operator for painting something."
  (fn [g object]
    (:type object)))

(defmethod paint :apple
  [g {:keys [location colour]}]
  (fill-point g location colour))

(defmethod paint :snake
  [g {:keys [body colour]}]
  (doseq [point body]
    (fill-point g point colour)))

;;;-------------------------------------
;;; Making the game panel

(defn game-panel
  "Our game panel will be made inside a frame. Here, we deal with the events that we ned to listen to."
  [frame snake apple]
  ;; Proxy creates an anonymous java class, used once.
  (proxy [JPanel ActionListener KeyListener] ;; Extends these objects
      [] ;; No arguments needed for the constructor
    ;;; Methods begin here
    ;; JPanel
    (paintComponent [g]
      (proxy-super paintComponent g)
      ;; We use proxy-super to bring in the inherited version of paint-component.
      ;; The inherited paint-component blanks out the whole panel.
      ;; Once the canvas is blanked, print the rest of the state.
      (paint g @apple)
      (paint g @snake))

    (getPreferredSize []
      (Dimension. (* (inc field-width) point-size)
                  (* (inc field-height) point-size)))

    ;; ActionListener
    (actionPerformed [event]
      ;; This is invoked every time the game is updated.
      ;; We actually don't care about our "event" parameter but ActionListener requires it.
      (update-positions snake apple)
      (cond (lose? @snake) (do (reset-game snake apple)
                               (JOptionPane/showMessageDialog frame "You lose!"))
            (win? @snake) (do (reset-game snake apple)
                              (JOptionPane/showMessageDialog frame "You win!")))
      (.repaint this))

    ;; KeyListener
    (keyPressed [event]
      (let [direction (directions (.getKeyCode event))]
        (if direction
          (update-direction snake direction))))

    ;; Ignore these events. Unfortunately this is required for the KeyListener interface
    (keyReleased [event])
    (keyTyped [event])))

;;;;=================================
;;;; MAIN

(defn snake-game
  []
  (let [snake (ref (create-snake))
        apple (ref (create-apple))
        frame (JFrame. "Snake")
        panel (game-panel frame snake apple)
        timer (Timer. turn-millis panel)]
    (.setFocusable panel true)
    (.addKeyListener panel panel)

    (.add frame panel) ;; Add all the
    (.pack frame) ;; Size and position
    (.setDefaultCloseOperation frame JFrame/EXIT_ON_CLOSE)
    (.setVisible frame true)

    (.start timer)))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (snake-game))
